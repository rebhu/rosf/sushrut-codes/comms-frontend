const
  express = require('express'),
  serveStatic = require('serve-static'),
  port = process.env.PORT || 5000,
  proxyHost = process.env.PROXY_HOST || 'comms-backend',
  proxyPort = process.env.PROXY_PORT || 4000

const app = express()

app.use(serveStatic('./dist/spa'))

const proxy = require('http-proxy-middleware')

app.use('/api', proxy({
  target: `http://${proxyHost}:${[proxyPort]}`,
  pathRewrite: { '^/api': '' }
}))

app.listen(port)
